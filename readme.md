# txtMailer

Created by [txtbox](https://archive.is/lRKVm) who now runs [cbox](http://cbox.ws)
Maintained by [desbest](http://desbest.com)


txtMailer is a simple PHP emailer that supports any 
number and combination of fields. Use it as a 'contact us' 
form to hide your email address, or to receive survey 
responses, etc.

## [ CHANGELOG ]
v1 --> Created
v2 --> Original txtbox authored version
v3 --> Error handling, bold formatting, stating sender email in body, tidier code
v4 --> Enable javascript alert once email is sent.
v5 --> Ajax support, confirmation email.

## [ INSTALLATION ]
 
 * Extract the txtmailer.php file to some place on your 
   hard drive.
 * Open txtmailer.php in a text editor, and change the 
   three variables near the top.
 * Reference txtmailer.php in your mailer form.
 * Upload txtmailer.php to your website.
 
## [ USAGE ]

Look at example.html for a simple form that you can 
use as reference. 

Note that the three variables you set in txtmailer.php 
can be overridden by fields in the form - useful if 
you use more than one mailer form on your site.

    ------> How do I override them?
    Having an form field named "email", "subject", or
    "returnto" overrides the default variables of
    txtMailer.

Also included is a small JavaScript function for checking 
the email field and any other required fields.
