<?php
/***********
    txtMailer v5
    by desbest
    http://github.com/desbest
*/
    
// Default settings. Change these!
$recipient = 'recipient@example.com';
$subject = 'Website Feedback';
$returnto = 'none';	// Default page people should be redirected to after mailing
                    	// making the value "none" will make no redirect (for ajax)
$returnto = './;	// Default page people should be redirected to after mailing
$sentalert === false; 	// Show an alert once email is sent? true or false
$confirmationEmail = true; $websiteName = "Your website name"; $domainroot = "example.com"; //no http://www.
$confirmationSubject = "Your email has been sent to $websiteName";
////__end_________________________________________||

// Override defaults
$subject = ($_POST['subject'])?$_POST['subject']:$subject;
$returnto = ($_POST['returnto'])?$_POST['returnto']:$returnto;

// if email address isn't present or is invalid, use TO address as FROM address.
$email = str_replace("\n", '_', strtolower(substr(stripslashes($_POST['email']), 0, 50)));
if (eregi('^.+@.+\.[a-z]{2,6}', $email)) {
  $message = "$websiteName feedback from ".$email."<br/><br/>";
          $confirmationMessage = "Your email has been sent to $websiteName <br/><br/>";
}
else {
  //$email = $recipient;
    $email = $_POST['email1'];
    $message = "$websiteName feedback from Anonymous<br><br>";
            $confirmationMessage = "Your email has been sent to $websiteName <br/><br/>";
}
foreach ($_POST as $key => $val) {
  if ($key != 'returnto' && $key != 'email' && $key != 'subject' && $key != 'sendto') {
    $message .= "<b>".$key.": </b>".$val."<br>";
            $confirmationMessage .= "<b>".$key.": </b>".$val."<br>";
  }
}
$message = stripslashes($message);
        $confirmationMessage .= "<br/><br/><a href=\"http://$rootdomain\" target=\"_blank\">Return to $websiteName</a>";
        $confirmationMessage = stripslashes($confirmationMessage);

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= "To:<$recipient>" . "\r\n";
$headers .= "From:<$email>" . "\r\n";
        $confirmationHeaders  = 'MIME-Version: 1.0' . "\r\n";
        $confirmationHeaders .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $confirmationHeaders .= "To:<$email>" . "\r\n";
        $confirmationHeaders .= "From: $websiteName <donotreply@$domainroot>" . "\r\n";


if (@mail($recipient, $subject, $message,$headers)) {
  if ($returnto == "none"){
    //do nothing
  } 
  elseif ($returnto) {
    if ($sentalert === true){ echo "<script type=\"text/javascript\">alert(\"Your email was sent!.\")</script>"; }
    header('Location: '.$returnto);
  } else {
    ?><html><body><center><font color="green"><b>email sent!</b></font><p><a href="JavaScript:history.back()">Back</a></center></body></html><?
  }
} 
else {  		
    $error = error_get_last(); $errormessage = $error['message'];
    ?>
    <script type="text/javascript">alert("Your email was not sent. Technical Difficulites! \n<?php echo $errormessage; ?>"); window.history.back();</script>
	<?php
}

/* Send the confirmation email */
if (@mail($email, $confirmationSubject, $confirmationMessage,$confirmationHeaders)) {
  if ($sentalert === true){ echo "<script type=\"text/javascript\">alert(\"Your email was sent!.\")</script>"; }
  if ($returnto == "none"){
    //do nothing
  } 
  elseif ($returnto) {
    header('Location: '.$returnto);
  } else {
    ?><html><body><center><font color="green"><b>email sent!</b></font><p><a href="JavaScript:history.back()">Back</a></center></body></html><?
  }
} 
else {  		
    $error = error_get_last(); $errormessage = $error['message'];
    ?>
    <script type="text/javascript">alert("Error sending you a confirmation mail. Technical Difficulites! \n<?php echo $errormessage; ?>"); window.history.back();</script>
	<?php
}
?>
