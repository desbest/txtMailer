<?php

// Default settings. Change these!
$address = 'address@your-domain.com';		// Default address you want mail sent to
$subj = 'site feedback';			// Default subject to give to emails
$returnto = 'http://mysite.com/thanks.html';	// Default page people should be redirected to after mailing


//////////////

// Override defaults
$subj = ($_POST['subject'])?$_POST['subject']:$subj;
$returnto = ($_POST['returnto'])?$_POST['returnto']:$returnto;

// if email address isn't present or is invalid, use TO address as FROM address.
$email = str_replace("\n", '_', strtolower(substr(stripslashes($_POST['email']), 0, 50)));
if (eregi('^.+@.+\.[a-z]{2,6}', $email)) {
  $message = "Auto Mail from ".$email."\n\n";
}
else {
  $email = $address;
  $message = "Auto Mail from Anonymous\n\n";
}
foreach ($_POST as $key => $val) {
  if ($key != 'returnto' && $key != 'email' && $key != 'subject' && $key != 'sendto') {
    $message .= $key.': '.$val."\n";
  }
}
$message = stripslashes($message);
if (@mail($address, $subj, $message,"From: ".$email)) {
  if ($returnto) {
    header('Location: '.$returnto);
  }
  else {
    ?><html><body><center><font color="green"><b>email sent!</b></font><p><a href="JavaScript:history.back()">Back</a></center></body></html><?
  }
} 
else {
  ?><html><body><center><font color="red"><b>there was a problem sending the mail</b></font><p><a href="JavaScript:history.back()">Back</a></center></body></html><?
}

?>
